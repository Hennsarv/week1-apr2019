﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace KaardipakiAlgus
{
    class Program
    {
        static void Main(string[] args)
        {
            List<int> pakk = Enumerable.Range(0, 52).ToList();

            int i = 0;
            foreach(var x in pakk)
                Console.Write($"{x}{(++i%13==0?"\n":"\t")}");
        }
    }
}
