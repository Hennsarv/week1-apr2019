﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace ForEachTsükkel
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = { 7, 2, 3, 8, 1, 9, 10 };

            for (int i = 0; i < arvud.Length; i++)
            {
                arvud[i] *= 2;
                Console.WriteLine($"arv nr {i} on {arvud[i]}");
            }

            foreach(var x in arvud)
            {
                Console.WriteLine(x);
            }
        }
    }
}
