﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace NeljapäevaneHarjutus1variant2
{
    class Program
    {
        static void Main(string[] args)
        {
            Random r = new Random(); // see on pisut uus vigur
                                     // see annab juhuslikke arve
                                     // sama mis eelmine, aga massiivi asemel võtame arvud "laest"
            int vastuseid = 0;
            int küsimusi = 0;        // kuna me ei tea, mitu küsimust tuleb, siis hindamiseks
                                     // on vaja ka neid loendada

            while (true)      // kasutame lõpmatut tsüklit, ku ta enam ei taha, siis katkestame break;
            {
                int vastus;   // muutuja kuhu loeme vastuse (jälle teeme ENNE tsüklit valmis)

                küsimusi++;   // loendame küsimuste arvu
                int esimene = r.Next(20);  // anna mulle juhulik arv kuni 20ni
                int teine = r.Next(20);    // teine samasugune - nüüd on kaks juhuslikku arvu 0..19
                do
                {
                    Console.Write($"kaks arvu {esimene} ja {teine}, mis on korrutis: ");
                    vastus = int.Parse(Console.ReadLine());
                    vastuseid++;   // loendame vastuseid
                }
                while (vastus != esimene * teine);
                Console.Write("veel: ");     // kui saime (lõpuks) õige vastuse, küsime kas tahab veel
                if (Console.ReadLine() != "") break; // kui ütellb midagi muud kui tühi rida, siis pooleli
            }

            // hindamine - muid nagu ennegi, aga massiivi pikkuse asemel on meil küsimiste loendur
            int tulemus = küsimusi * 100 / vastuseid;
            Console.WriteLine($"tulemus {tulemus} küsimusi {küsimusi} vastuseid {vastuseid}");
            if (tulemus >= 90) Console.WriteLine("hinne 5");
            else if (tulemus >= 80) Console.WriteLine("hinne 4");
            else if (tulemus >= 70) Console.WriteLine("hinne 3");
            else if (tulemus >= 60) Console.WriteLine("hinne 2");
            else Console.WriteLine("hinne 1");

        }
    }
}
