﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;

namespace Tsüklid
{
    class Program
    {
        static void Main(string[] args)
        {
            int[] arvud = {  1, 3, 2, 4, 5, 13, 8, 7, 1, 9 };

            // näe siin on arvud - kirjuta tsükkel, mis
            // * leiab nende arvude summa, 
            // * keskmise ja 
            // * suurima arvu massiivis
            // ja seejärel trükib nad välja
            // Selle massiivi summa on 53 keskmine on 5,3 ja suurim on 13

            // 1. alustame kõige lihtsamast summa
            // 2. keskmise saime ka kuidagi
            // aga kuidas leida kõige suurem

            if (arvud.Length > 0)
            {
                int summa = 0;
                int suurim = arvud[0];
                for (int i = 0; i < arvud.Length; i++)
                {
                    summa += arvud[i];
                    if (arvud[i] > suurim) { suurim = arvud[i]; }
                    //suurim = arvud[i] > suurim ? arvud[i] : suurim;
                }

                Console.WriteLine($"summa on {summa} keskmine on { (double)summa / arvud.Length } suurim on {suurim}");

            }
            else
            {
                Console.WriteLine("see massiiv on tühi ja summa on 0");

            }

            Console.WriteLine($"summa on {arvud.Sum()} keskmine on {arvud.DefaultIfEmpty().Average()} suurim on {arvud.DefaultIfEmpty().Max()}");

            // summad arvutad läheme liiklusse

            string color = "";

            while (color != "green" && color != "roheline")
            {
                Console.Write("mis värvi sa näed (green, red, yellow): ");
                color = Console.ReadLine().ToLower();

                switch (color)
                {
                    case "red":
                    case "punane":
                        Console.WriteLine("jää seisma");
                        break;
                    case "green":
                    case "roheline":
                        Console.WriteLine("sõida edasi");
                        break;
                    case "yellow":
                    case "kõllane":
                        Console.WriteLine("oota rohelist");
                        break;
                    default:
                        Console.WriteLine("pese silmad");
                        break;
                } 
            }

            // jõudsime koju (või pubisse)

            string vastus = "";
            do

            {
                Console.WriteLine("joo see õlu");
                Console.Write("kas tahad veel: ");
                vastus = Console.ReadLine();
            }
            while (vastus != "ei");




        }
    }
}
