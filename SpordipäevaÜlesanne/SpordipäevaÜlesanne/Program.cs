﻿using System;
using System.Collections.Generic;
using System.Linq;
using System.Text;
using System.Threading.Tasks;
using System.IO;

namespace SpordipäevaÜlesanne
{
    class Program
    {
        static void Main(string[] args)
        {
            string failinimi = @"..\..\spordipäeva protokoll.txt";
            string[] loetudRead = File.ReadAllLines(failinimi);
            foreach (var r in loetudRead) Console.WriteLine(r);
            List<String> nimed = new List<string>();
            List<int> distantsid = new List<int>();
            List<int> ajad = new List<int>();
            List<double> kiirused = new List<double>();

            for (int i = 1; i < loetudRead.Length; i++)
            {
                string rida = loetudRead[i]; // see on üks rida
                string[] osad = rida.Split(','); // tükeldatud rida
                nimed.Add(osad[0]);
                distantsid.Add(int.Parse(osad[1].Trim()));
                ajad.Add(int.Parse(osad[2].Trim()));
                kiirused.Add(distantsid[i - 1] * 0.1 / ajad[i - 1]);
            }

            Double kiireim = kiirused.Max();
            for (int i = 0; i < nimed.Count; i++)
            {
                if (kiirused[i] == kiireim)
                { Console.WriteLine($"Kiireim oli {nimed[i]} tema kiirus oli {kiirused[i]}"); }
            }
            
        }
    }
}
